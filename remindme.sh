#!/bin/bash

#rememberfile
rememberfile="$HOME/.remember"
#conditional for if there is no rememberfile
if [ ! -f $rememberfile ] ; then
 echo "$0: You don't seem to have a .remember file. " >&2
 echo "To remedy this, please use 'remember' to add reminders" >&2
 exit 1
fi
#conditional that displays entire rememberfile
if [ $# -eq 0 ] ; then
more $rememberfile
#conditional that searchs for and returns line of specific terms
else grep -i -- "$@" $rememberfile | ${PAGER:-more}
fi
exit 0
