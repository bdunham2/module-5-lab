#!/bin/bash

#file that stores notes that will be saved by user
rememberfile="$HOME/.remember"
# conditional 
if [ $# -eq 0 ]
then echo "Enter note, end with ^D: "
#notes are saved in rememberfile
cat - >> $rememberfile
#notes are appended
else
 # Append any arguments passed to the script on to the .remember file.
echo "$@" >> $rememberfile
fi
exit 0
